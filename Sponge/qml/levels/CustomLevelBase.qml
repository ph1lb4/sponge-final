import VPlay 1.0
import QtQuick 1.1
import "../entities"

LevelBase {
    anchors.fill: parent;

    height: 30000

    Behavior on y {
        // the cross-fade animation should last 500ms
        SmoothedAnimation {duration: 500}
    }

    //physics
    property variant gravity;
    //property variant wind;

    property variant floorTextureSource;
    property variant backgroundSource;
    property variant gravityY;

    onVisibleChanged: {

    }
}
