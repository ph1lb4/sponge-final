import VPlay 1.0
import QtQuick 1.1
import "../images/"

CustomLevelBase {
    id: earthLevel
    floorTextureSource: "../images/ground.png"
    backgroundSource: "../images/earthBackground-sd.png"
    //parallexBack: "../images/parallexBackEarth.png"
    gravity: -30

}
