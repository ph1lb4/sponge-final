import VPlay 1.0
import QtQuick 1.1
import VPlay.plugins.gamecenter 1.0
import VPlay.plugins.store 1.0
import "scenes"
import "controllers"
import "entities"
import "levels"
import "js/GameLogic.js" as GameLogic

GameWindow {

    id: window

    property bool sound: true;
    property int currentHighscore: 0;

    // enable FPS in release mode
    displayFpsEnabled: false
    activeScene: splashscreenScene
    state: "splashscreenState" // CHANGE TO "gameSceneState" STATE TO SEE THE GAMESCENE AT STARTUP

    width: 320
    height: 480

    onSoundChanged: {
        soundController.updateSound();
    }


    Component.onCompleted: {
        console.debug("GAME STARTED.");
        gameCenter.authenticateLocalPlayer();
        spongeController.initialize();
        console.debug(spongeStore.printStoreProductLists());
        soundController.backgroundMusic.play();
    }

    GameCenter{
        id: gameCenter;

        onAuthenticatedChanged:{
            if (authenticated === true) {
                console.log("Authentication status: " + authenticated);
                var gameCenterScore = getGameCenterScore();
                if (gameCenterScore > currentHighscore){
                    currentHighscore = gameCenterScore;
                }
            }
        }
    }

    // the result of the input dialog is received with a connection to the signal textInputFinished
    Connections {
        target: nativeUtils
    }

    SpongeController {
        id: spongeController
        Component.onCompleted: initialize();
    }

    FontLoader {
        id: fontLoader
        source: "fonts/shininglikestars.ttf"

        Component.onCompleted: {
            if(system.platform === System.Android){
                name = "Shining Like Stars"
            }
        }
    }

    SplashscreenScene {
        id: splashscreenScene
        opacity: 0
    }

    MainMenuScene {
        id: mainMenuScene
        opacity: 0
    }

    GameScene {
        id: gameScene
        opacity: 0
    }

    MySpongeScene {
        id: mySpongeScene
        opacity: 0
    }

    IngameStoreScene {
        id: ingameStoreScene
        opacity: 0
    }

    LeaderboardScene {
        id: leaderboardScene
        opacity: 0
    }

    GameOverScene {
        id: gameOverScene
        opacity: 0
    }

    EntityManager {
        id: entityManager
        entityContainer: gameScene
    }

    EntityManager {
        id: mainMenuEntityManager
        entityContainer: mainMenuScene
    }

    MobileInputController{
        id: mobileInputCtrl
    }

    Keys.forwardTo: desktopInputCtrl;

    DesktopInputController{
        id: desktopInputCtrl
    }

    SoundController {
        id: soundController
    }

    Store {
        id: spongeStore

        version: 1
        // Replace with your own custom secret
        secret: "com.xxmasseveloper.sponge.store.secret"
        // From Google Play Developer Console
        androidPublicKey: "shit"

        goods: [
            SingleUseGood {
                id: skillPoint
                itemId: "skill_point_id"
                name: "Skill Point"
                description: "A skill point to enhance your sponges abilities"
                purchaseType: StorePurchase { id: skillPointPurchase; productId: skillPoint.itemId; price: 0.89;}
            }
        ]

        onStorePurchased: {
            if(itemId === skillPoint.itemId){
                spongeController.increaseAvailableSkillPoints();
            }
        }

        onStorePurchaseStarted: {
            console.debug("Store purchases started with id:", itemId)
        }

        onStorePurchaseCancelled: {
            console.debug("Store purchases cancelled with id:", itemId)
        }

        onRestoreAllTransactionsFinished: {
            console.debug("Restore all transactions succeeded:", success)
        }

        onRestoreAllTransactionsStarted: {
            console.debug("Restore all transactions started")
        }

        onItemNotFoundError: {
            console.debug("Item for transaction not found:", itemId)
        }

        onInsufficientFundsError: {
            console.debug("Not enough gold nuggets for that one...")

            nativeUtils.displayMessageBox("Out of Gold", "You're out of gold, you can however purchase some nuggets again.")
        }
    }

    states: [
        State {
            name: "gameSceneState"
            StateChangeScript {
                script: {
                    activeScene.hide();
                    gameScene.show();
                    window.activeScene = gameScene;
                    gameScene.loadLevel("../levels/Earth.qml");
                    gameScene.gameStarted = true;
                    gameScene.reset();
                    gameScene.startLevel();
                }
            }
        },
        State {
            name: "splashscreenState"
            StateChangeScript {
                script: {
                    activeScene.hide();
                    splashscreenScene.show();
                    window.activeScene = splashscreenScene;
                }
            }
        },
        State {
            name: "mainMenuState"
            StateChangeScript {
                script: {
                    activeScene.hide();
                    mainMenuScene.show();
                    //                    mainMenuScene.createMenuButtons();
                    //                    mainMenuScene.physicsEnabled = false;
                    //                    mainMenuScene.physicsEnabled = true;
                    window.activeScene = mainMenuScene;
                    gameScene.gameStarted = false;
                }
            }
        },
        State {
            name: "mySpongeSceneState"
            StateChangeScript {
                script: {
                    activeScene.hide();
                    mySpongeScene.show();
                    window.activeScene = mySpongeScene;
                }
            }
        },
        State {
            name: "ingameStoreSceneState"
            StateChangeScript {
                script: {
                    activeScene.hide();
                    ingameStoreScene.show();
                    window.activeScene = ingameStoreScene;
                }
            }
        },
        State {
            name: "leaderboardSceneState"
            StateChangeScript {
                script: {
                    activeScene.hide();
                    leaderboardScene.show();
                    window.activeScene = leaderboardScene;
                }
            }
        },
        State {
            name: "gameOverSceneState"
            StateChangeScript {
                script: {
                    activeScene.hide();
                    gameOverScene.show();
                    window.activeScene = gameOverScene;
                    // gameScene.gameOver();
                }
            }
        }

    ]
}

