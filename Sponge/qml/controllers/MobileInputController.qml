import VPlay 1.0
import QtQuick 1.1

Item {

//    signal backPressed;

//    Keys.onPressed: {
//      console.debug("pressed key id: ", event.key)

//      if(event.key === Qt.Key_R) {
//          console.debug("Gamescene restart!");
//          level.restartGame();
//          gamescene.animateRestartText();
//          gamescene.state = ""
//      }
//    }

//    Keys.onBackPressed: {
//      console.debug("the back button was pressed on Android, handle the signal in the scene")
//      backPressed()
//    }

    Accelerometer {
        active: !system.desktopPlatform
        filterFactor: 0
        onReadingChanged: {
//            if(activeScene === gameScene || activeScene === mainMenuScene) {
            if(activeScene === gameScene) {
                activeScene.xMovement(reading.x * 2);
            }
        }
    }

}

