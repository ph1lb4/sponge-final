import VPlay 1.0
import QtQuick 1.1


Item {

    signal backPressed;

    Keys.onPressed: {

        //needed instead of other if, when sponge should move in mainMenuScene
        //if(activeScene === gameScene || activeScene === mainMenuScene) {
        if(activeScene === gameScene) {
            switch(event.key){
            case Qt.Key_Backspace:
                if(system.desktopPlatform){
                    backPressed();
                }
                break;
            case Qt.Key_Up:{
                //console.debug("DESKTOP INPUT CONTROLLER::::UP PRESSED");
                activeScene.jump();
            }
                break;
            case Qt.Key_Left:{
                //console.debug("DESKTOP INPUT CONTROLLER::::LEFT BREAST");
                activeScene.playerSponge.controllerX = -1;
            }
                break;
            case Qt.Key_Right:{
                //console.debug("DESKTOP INPUT CONTROLLER::::RIGHT BREAST");
                activeScene.playerSponge.controllerX = 1;
            }
                break;
            case Qt.Key_R:{
                activeScene.gameOver();
                console.debug("GAME OVER DEBUG PRESSED")
            }
                break;
            }
        }
    }

    Keys.onReleased: {
//        if(activeScene === gameScene || activeScene === mainMenuScene) {
        if(activeScene === gameScene) {
            switch(event.key){
            case Qt.Key_Left:{
                //console.debug("DESKTOP INPUT CONTROLLER::::LEFT RELEASED");
                activeScene.playerSponge.controllerX = 0;
            }
                break;
            case Qt.Key_Right:{
                //console.debug("DESKTOP INPUT CONTROLLER::::RIGHT RELEASED");
                activeScene.playerSponge.controllerX = 0;
            }
                break;
            }
        }
    }


}
