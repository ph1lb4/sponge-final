import VPlay 1.0
import QtQuick 1.1

Item {

    property alias backgroundMusic: backgroundMusic
    property alias clickSound: clickedSound
    property alias jumpSound: jumpSound
    property alias gameOverSound: gameOverSound

    BackgroundMusic {
        id: backgroundMusic
        //autoPlay: false
        source: "../sounds/background.mp3"
        volume: sound? 0.3:0
        // the BackgroundMusic will automatically be played, because the autoPlay property is set to true by default
    }

    Sound{
        id:gameOverSound
        volume: 1
        source: "../sounds/game_over.mp3"
        onPlayingChanged: {
            if(playing){
                backgroundMusic.volume = 0.07
                threeSecTimer.start();
            }
        }
    }

    Timer{
        id: threeSecTimer
        interval: 3000
        repeat: false
        running: false

        onTriggered: {
            backgroundMusic.volume = 0.3
            threeSecTimer.stop();

        }
    }

    Sound{
        id: clickedSound
        volume: 2
        source: "../sounds/click.mp3"
    }

    Sound{
        id: jumpSound
        volume: 0.1
        source: "../sounds/jump.wav"
    }

    function updateSound(){
        backgroundMusic.volume = sound? 0.3:0;
    }

}
