import VPlay 1.0
import QtQuick 1.1

Item {

    property variant jumpingType: "jumpSkillType"
    property variant waterType: "waterSkillType"
    property variant stickyType: "stickySkillType"

    property variant spongeSkin: "../images/skin04.png" // default value skin04 = sponge

    property int maxSkillLevel: 10

    property int jumpingSkill: 0 // 0-maxSkillLevel
    property int waterresistanceSkill: 0
    property int stickinessSkill: 0

    // watch out, this is a double
    property double availableSkillPoints: availableFragments / 5 // available skillpoints is always tied to the available fragments
    property int availableFragments: 10

    property int health: 100 // 0-100
    property int lives: 10 // default 10

    property variant activePowerUp: "none"

    // read all values from the settings
    function initialize() {

        readSkill(jumpingType);
        readSkill(waterType);
        readSkill(stickyType);

       // readAvailableSkillPoints();
        readAvailableFragments();

        //readSpongeSkin();

        readLives();
    }

    // call this whenever a life is lost
    function loseLife() {
        lives--;
        storeLives();
    }

    // read the current lives from the settings
    function readLives() {

        var value = settings.getValue("sponge_lives");
        if(value) {

            lives = value;
        } else {
            console.debug("READING LIVES undefined / null");
        }

        console.debug("Reading LIVES: ", value);
    }

    // store the current lives into the settings
    function storeLives() {
        settings.setValue("sponge_lives", lives);
    }

    // reset the health/soakedness to full - everytime a new game starts the heath/soakedness recharges
    function resetHealth() {
        health = 100;
    }

    // save the current sponge skin in the settings
    function storeSpongeSkin() {
        settings.setValue("spongeSkinPath", spongeSkin);
    }

    // reads the spongeskin from the settings
    function readSpongeSkin() {

        var value = settings.getValue("spongeSkinPath");
        if(value && value !== "undefined") {

            spongeSkin = value;
        } else {
            spongeSkin = "../images/skin04.png";
        }

        console.debug("Reading SPONGESKIN: ", value);
    }

    function selectSkin(selectedSpongeSkin) {
        console.debug("SELECTED SPONGE SKIN:", selectedSpongeSkin);
        spongeSkin = selectedSpongeSkin;
        //storeSpongeSkin();
    }

    // returns the skill level for a specific skilltype
    function getSkillLevel(skillType) {
        var skill = 0;
        switch(skillType) {
        case jumpingType:
            skill = jumpingSkill;
            if(activePowerUp === "jump"){
                skill = skill * 1.3;
            }
            break;
        case waterType:
            skill = waterresistanceSkill;
            break;
        case stickyType:
            skill = stickinessSkill;
            break;
        }
        return skill;
    }

    // increases the skill level of a specific skill and stores its value in the settings
    function increaseSkill(skillType) {

        // check if skillpoints are available to distribute
        if(availableSkillPoints >= 1) {

            // check if the maximum level for that skill is not reached yet
            if(getSkillLevel(skillType) < maxSkillLevel) {

                switch(skillType) {
                case jumpingType:
                    jumpingSkill++
                    break;
                case waterType:
                    waterresistanceSkill++
                    break;
                case stickyType:
                    stickinessSkill++
                    break;
                }

                availableFragments -= 5;
                storeSkill(skillType);
                storeAvailableFragments();
            }
        }
    }

    // save the currently available skillpoints
//    function storeAvailableSkillPoints() {
//        settings.setValue("availableSkillPoints", availableSkillPoints);
//    }

    // reads the available skillpoints from the settings
//    function readAvailableSkillPoints() {

//        var value = settings.getValue("availableSkillPoints");
//        if(value && value !== "undefined") {

//            availableSkillPoints = value;
//        } else {
//            console.debug("SKILLPOINTS undefined / null");
//            availableSkillPoints = 0;
//        }

//        console.debug("Reading SKILLPOINTS: ", value);
//    }

    // saves the current skill level in the settings, the skilltype is the key / identifier
    function storeSkill(skillType) {

        switch(skillType) {
        case jumpingType: settings.setValue(skillType, jumpingSkill); break;
        case waterType: settings.setValue(skillType, waterresistanceSkill); break;
        case stickyType: settings.setValue(skillType, stickinessSkill); break;
        }
    }

    // reads a skill from the settings and stores it into the right variable
    function readSkill(skillType) {

        var value = 0;

        switch(skillType) {
        case jumpingType: {

            value = settings.getValue(skillType);
            if(value && value !== "undefined") {

                jumpingSkill = value;
            } else console.debug("READING SKILL undefined / null");
        } break;
        case waterType: {

            value = settings.getValue(skillType);
            if(value && value !== "undefined") {

                waterresistanceSkill = value;
            } else console.debug("READING SKILL undefined / null");
        } break;
        case stickyType: {

            value = settings.getValue(skillType);
            if(value && value !== "undefined") {

                stickinessSkill = value;
            } else console.debug("READING SKILL undefined / null");
        } break;
        }

        console.debug("Reading SKILL: ", value);
    }

    function readAvailableFragments() {

        var value = settings.getValue("availableFragments");
        if(value && value !== "undefined") {

            availableFragments = value;
        } else {
            console.debug("FRAGMENTS undefined / null");
            availableFragments = 0;
        }

        console.debug("Reading FRAGMENTS: ", value);
    }

    function storeAvailableFragments() {
        settings.setValue("availableFragments", availableFragments);
    }

    function increaseAvailableFragments(increaseamount) {

        availableFragments = availableFragments + increaseamount;
        storeAvailableFragments();
    }

    function increaseAvailableSkillPoints(increaseamount) {

        // skillpoints depend on fragments, therefore increase fragments
        availableFragments = availableFragments + 5*increaseamount;
        storeAvailableFragments();
    }

    function getJumpSkin() {
        var skin = spongeSkin + "";
        var length = skin.length;
        var jumpSkin = "../images/" + skin.substring(length-10, length-4) + "_jump.png";
        console.debug("SKIN PATH: ", jumpSkin);
        return jumpSkin;
    }

    function loadHighScore() {

        var highscore = settings.getValue("highscore");

        if(highscore && highscore !== "undefined") {
            return highscore;
        } else return 0;
    }

    function storeHighScore(highscore) {
        settings.setValue("highscore", highscore);
    }

    function reset() {

        jumpingSkill = 0;
        storeSkill(jumpingType);
        waterresistanceSkill = 0;
        storeSkill(waterType);
        stickinessSkill = 0;
        storeSkill(stickyType);

        resetHealth();

        //availableSkillPoints = 10;
        //storeAvailableSkillPoints();
        availableFragments = 0;
        storeAvailableFragments();

        spongeSkin = "../images/skin04.png";
        storeSpongeSkin();

        lives = 10;
        storeLives();
    }
}
