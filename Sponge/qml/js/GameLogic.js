// VARIABLES FOR CURRENT GAME STATE
var curFloorOffset;
var lastBlockCollision;
var spongeOnFloor;
var actualScore = 0;

var jumpSkill = "jumpSkillType";
var stickSkill = "stickySkillType";
var waterSkill = "waterSkillType";

var sceneHeight;
var floorHeight;
var floor;


var levelMoving = false;

var debugInitDone = false;

/**
  * Resets the game logic and game scene relevant parts using the reset method.
  */
function startGame(){
    reset();
}

/**
  * Resets the game logic and game scene relevant parts. Seperate method just to logically differ start/reset in calling.
  */
function reset(){
    var sponge = gameScene.playerSponge;
    sceneHeight = gameScene.height;
    floor = gameScene.floor;
    floorHeight = gameScene.floor.height;
    curFloorOffset = 0;
    lastBlockCollision = -1;
    actualScore = 0;
    sponge.y = sceneHeight * 0.6;
    sponge.x = gameScene.width / 2 - sponge.width / 2;
    gameScene.water.y = sceneHeight;
    spongeController.resetHealth();
}

/**
  * Returns the current score of the sponge to be shown in hud.
  */
function getCurrentScore(){
    var sponge = gameScene.playerSponge;
    var score = gameScene.score;
    if(!levelMoving){
        var zeroLevel = sceneHeight - floorHeight + curFloorOffset;
        var spongeLevel = sponge.y + sponge.height;
        score = zeroLevel - spongeLevel;
        score = score / 5;
        if(score < 0){
            // can happen because of blockcolliders that are slightly smaller than the objects
            score = 0;
        }

        if(actualScore < score){
            actualScore = score;
            updateHUDwithActualScore();
        }
        // update HUD if new maxlevel has been reached
        gameScene.updateCurrentScore(score);
    }

    return score;
}

function updateHUDwithActualScore(){
    gameScene.score = actualScore;
    gameScene.updateActualScore(actualScore);
}

/**
  * Handles collisions of the sponge with other object. The collided object is passed in as parameter.
  */
function onCollisionDetected(collidedObj, contactNormal){
    var sponge = gameScene.playerSponge;
    var collidingType = collidedObj.owningEntity.entityType;
    var collidedEntity = collidedObj.parent;

    console.debug("Sponge colliding with ", collidingType);
    // only handle collisions if game is running, not in menus etc.
    if(collidingType === "block") {
        // check collision side
        var currentBlockCollisionType = getBlockCollisionType(collidedEntity, contactNormal);
        switch(currentBlockCollisionType){
        case 1: // top of block
            // sponge stands on block, nothing else to do then allow him to jump again
            //console.debug("Sponge collided with block TOP");
            gameScene.spongeCanJump = true;
            sponge.textureSource = spongeController.spongeSkin;
            adaptBlockLevelToSponge();

            // sponge can move left/right --> reset stickiness
            resetStickiness();
            lastBlockCollision = currentBlockCollisionType;
            break;
        case 2: // left side
            // sponge is sliding down the side of a block, he can jump here as well
            //console.debug("Sponge collided with block LEFT");
            if(!gameScene.spongeCanJump){
                if(lastBlockCollision === -1){
                    // block sliding down --> apply stickiness
                    applyStickiness(collidedEntity.type);
                }
                // sponge hitting side of block in the air -> save collision type
                lastBlockCollision = currentBlockCollisionType;
            }
            gameScene.spongeCanJump = true;
            sponge.textureSource = spongeController.spongeSkin;
            break;
        case 3: // right side
            // sponge is sliding down the side of a block, he can jump here as well
            //console.debug("Sponge collided with block RIGHT");
            if(!gameScene.spongeCanJump){
                if(lastBlockCollision === -1){
                    // block sliding down --> apply stickiness
                    applyStickiness(collidedEntity.type);
                }
                // sponge hitting side of block in the air -> save collision type
                lastBlockCollision = currentBlockCollisionType;
            }
            gameScene.spongeCanJump = true;
            sponge.textureSource = spongeController.spongeSkin;
            // block sliding down --> apply stickiness
            applyStickiness();
            break;
        case 4: // bottom side of block
            // maybe killed by the block :-)
            //console.debug("Sponge collided with block BOTTOM - lastBlockCollision ", lastBlockCollision, " - spongeOnFloor ", spongeOnFloor);
            if(lastBlockCollision === 1 || spongeOnFloor){
                collidedEntity.collider.bodyType = Body.Static;
                gameScene.gameOver();
            }
            break;
        case -1: // invalid type
            console.debug("BlockCollisionType detection failed.");
            break;
        }
    }else if(collidingType === "floor"){
        gameScene.spongeCanJump = true;
        sponge.textureSource = spongeController.spongeSkin;
        spongeOnFloor = true;
        // sponge can move left/right --> reset stickiness
        resetStickiness();
    }else if(collidingType === "water"){
        gameScene.gameOver();
    } else if(collidingType === "powerup") {
        console.debug("COLLISION WITH POWERUP:", collidedEntity.type);
        spongeController.activePowerUp = collidedEntity.type;
        gameScene.powerUpCollected(spongeController.activePowerUp);
    }
}

/**
  * Called when the collision of the sponge with another object ended. The collided object is passed in as parameter.
  */
function onContactEnded(collidedObj){
    var sponge = gameScene.playerSponge;
    var collidingType = collidedObj.owningEntity.entityType;
    var collidedEntity = collidedObj.parent;

    var spongeX = (sponge.x | 0);
    var spongeY = (sponge.y | 0);

    var objX = (collidedEntity.x | 0);
    var objY = (collidedEntity.y | 0);

    if(collidingType === "block"){
        if(lastBlockCollision === 1){
            // sponge was previously on top of block -> check if it has slided over the edge
            if(spongeX + sponge.width <= objX || spongeX >= objX + collidedEntity.width){
                //console.debug("3... 2.... 1.... BUNGEE!");
                gameScene.spongeCanJump = false;
            }
        }else if(lastBlockCollision === 2 || lastBlockCollision === 3 || lastBlockCollision === -1){
            resetStickiness();
        }

        // block contact ended -> reset variable with last block collision
        lastBlockCollision = -1;
    } else if (collidingType === "floor"){
        // sponge jumped up from floor
        spongeOnFloor = false;
    }
}

/**
  * Returns the type of collision: 1 = TOP, 2 = LEFT, 3 = RIGHT, 4 = BOTTOM, -1 = INVALID
  */
function getBlockCollisionType(collidedEntity, contactNormal){
    if(contactNormal.y === -1){
        console.debug("#################### BLOCK TOP COLLISION ###################");
        return 1;
    }else if(contactNormal.y === 1){
        console.debug("#################### BLOCK BOTTOM COLLISION ###################");
        return 4;
    }else if(contactNormal.x === 1){
        console.debug("#################### BLOCK LEFT COLLISION ###################");
        return 2;
    }else if(contactNormal.x === -1){
        console.debug("#################### BLOCK RIGHT COLLISION ###################");
        return 3;
    }
}

function checkSpongeY(){
    var sponge = gameScene.playerSponge;
    var spongeY = sponge.y;
    if(spongeY + sponge.height < sceneHeight * 0.2 || ((spongeY + sponge.height) > sceneHeight * 0.95)){
        var diff = (sceneHeight / 2) - spongeY + (sponge.height);
        adaptEntityLevel(diff);
    }
}

/**
  * Adapts the level of all entities (blocks, powerups) incl. floor and sponge with the given value.
  */
function adaptEntityLevel(diff){
    var sponge = gameScene.playerSponge;
    if(gameScene.gameStarted){
        levelMoving = true;
        gameScene.levelMovement();
        var blocks = entityManager.getEntityArrayByType("block");
        var powerups = entityManager.getEntityArrayByType("powerup");

        for(var i = 0; i < blocks.length; i++){
            blocks[i].y = blocks[i].y + diff;
        }
        for(var j = 0; j < powerups.length; j++){
            powerups[j].y = powerups[j].y + diff;
        }

        gameScene.waterTimer.stop();
        gameScene.water.y += diff;
        gameScene.waterTimer.start();
        floor.y += diff;
        curFloorOffset += diff;
        sponge.y += diff;
    }
}

function levelMoveDone(){
    levelMoving = false;
}

/**
  * Adapts the whole level (blocks incl. floor and sponge) based on the current sponge position that the sponge
  * ends up in the lower third of the screen.
  */
function adaptBlockLevelToSponge(){
    var sponge = gameScene.playerSponge;
    if(gameScene.gameStarted){
        var screenHeight = sceneHeight;
        var spongeY = sponge.y + sponge.height;

        if(spongeY <= screenHeight / 1.7){
            var diff = (screenHeight * 0.80) - spongeY;
            adaptEntityLevel(diff);
        }else if(spongeY > screenHeight * 0.95){
            var diff2 = (screenHeight * 0.5) - spongeY;
            // console.debug("DIFF2: ", diff2, " - curFloorOffset: ", curFloorOffset);
            if(diff2 * (-1) > curFloorOffset){
                diff2 = curFloorOffset * (-1);
            }
            // console.debug("USING DIFF VALUE: ", diff2);
            adaptEntityLevel(diff2);
        }
    }
}

/**
  * Calculates a linear impulse and applies it to the sponge's box collider to let him jump.
  */
function jump(){
    var sponge = gameScene.playerSponge;
    if(spongeCanJump){
        sponge.textureSource = spongeController.getJumpSkin();
        if(window.sound){
            soundController.jumpSound.play();
        }
        spongeCanJump = false;

        var jumpVal = spongeController.getSkillLevel(jumpSkill)*1;

        var increase = jumpVal*1 * 0.05;
        var jumpStrength = (1 + increase*1)*1;

        //jumpVal = 1;
        var impulsePow = gameScene.level.gravity * 1000 * jumpStrength;

        var xDir = 0;
        if(lastBlockCollision === 2){
            // on left side of block
            xDir = -7500;
            impulsePow *= 1.2;
        }else if(lastBlockCollision === 3){
            // on right side of block
            xDir = 7500;
            impulsePow *= 1.2;
        }

        var impulse = playerSponge.boxCollider.body.getWorldVector(Qt.point(xDir,impulsePow));
        playerSponge.boxCollider.body.applyLinearImpulse(impulse, playerSponge.boxCollider.body.getWorldCenter());
    }
}

function xMovement(value){
    var sponge = gameScene.playerSponge;
    sponge.controllerX = value;
}


function applyStickiness(blockType){
    var sponge = gameScene.playerSponge;
    console.debug("COLLIDED BLOCK TYPE: ", blockType);
    var stickVal = spongeController.getSkillLevel(stickSkill);
    console.debug("ACTIVE POWERUP WHILE COLLIDING: ", spongeController.activePowerUp);
    switch(spongeController.activePowerUp){
    case "magnet":
        if(blockType === "metal"){
            stickVal = 15;
        }
        break;
    case "glue":
        if(blockType === "wood"){
            stickVal = 15;
        }
        break;
    case "spikes":
        if(blockType === "ice"){
            stickVal = 20;
        }
        break;
    }
    // friction of 0 is not comfortable for gameplay
    if(stickVal === 0){
        stickVal = 0.2;
    }

    console.debug("APPLYING STICKINESS - friction value: ", stickVal);
    sponge.boxCollider.friction = stickVal;
}

function resetStickiness(){
    var sponge = gameScene.playerSponge;
    //console.debug("RESET STICKINESS TO 0.1!");
    sponge.boxCollider.friction = 0.1;
}
