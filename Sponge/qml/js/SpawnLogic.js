var availableBlockTypes = ["stone", "wood", "metal", "ice"];
var availablePowerUpTypes = ["jump", "spikes", "magnet", "glue"];
var availableBlockSizeDiv = [ 4, 6 ];


/**
  * Creates a block of random size (and type in the future) and spawns it at a random x-position. The method takes care that
  * a new block is spawn at an y-position that each block has to fall the same distance to ensure a stable game experience.
  */
function checkAndSpawn(){
    // check if game is running
    if(gameScene.gameStarted){
        var minY = getBlockLevel();
        var screenHeight = gameScene.height;

        // spawn the next block
        var minDiv = 0;
        var maxDiv = availableBlockSizeDiv.length;
        var randSize = gameScene.width / (availableBlockSizeDiv[(Math.floor(utils.generateRandomValueBetween(minDiv, maxDiv)))]);

        var xRange = gameScene.width - randSize;
        var randomX = Math.floor(utils.generateRandomValueBetween(randSize/2, xRange));

        // spawn above highest block
        var spawnY = minY - (4 * randSize);
        if(spawnY > 0){
            // but don't spawn inside screen
            spawnY = -randSize;
        }


       // console.debug("SPAWNING BLOCK at x = ", randomX, " with size ", randSize, "x", randSize);

        var maxIndex = availableBlockTypes.length;

        var index = Math.floor(utils.generateRandomValueBetween(0, maxIndex));
        console.debug("INDEX FOR TYPE: ", index);

        var selType = availableBlockTypes[index];
        console.debug("SELECTED TYPE: ", selType);

        console.debug("SPAWNING BLOCK at x = ", randomX, " with size ", randSize, "x", randSize);

        entityManager.createEntityFromUrlWithProperties(Qt.resolvedUrl("../entities/Block.qml"), {x: randomX, y: spawnY, width: randSize, height: randSize, type: selType});
    }
}

function spawnPowerup() {
    // check if game is running
    if(gameScene.gameStarted) {


        var minY = getBlockLevel();
        // spawn above highest block
        var spawnY = minY - (100);
        if(spawnY > 0){
            // but don't spawn inside screen
            spawnY = -50;
        }

        var powerUpType = Math.floor(utils.generateRandomValueBetween(0, availablePowerUpTypes.length));
        var randomX = Math.floor(utils.generateRandomValueBetween(0, gameScene.width - 30)); // 30 is the powerup width

//        while(lastBlockSpawnX + 20 < randomX && lastBlockSpawnX - 20 > randomX) {
//            randomX = Math.floor(utils.generateRandomValueBetween(0, gameScene.width - 30)); // 30 is the powerup width
//        }

        var selType = availablePowerUpTypes[powerUpType];

        entityManager.createEntityFromUrlWithProperties(Qt.resolvedUrl("../entities/PowerUp.qml"), {x: randomX, y: spawnY, type: selType });
    }
}

/**
  * calculates and returns random new spawn interval between two values
  */
function calculateNewRandomInterval(from, to) {
    return Math.floor(utils.generateRandomValueBetween(from, to))
}

/**
  * Returns the current maximul y level of all blocks.
  */
function getBlockLevel(){
    var blocks = entityManager.getEntityArrayByType("block");
    var minY = -1;
    var screenHeight = gameScene.height;

    for(var i = 0; i < blocks.length; i++){
        if(minY === -1){
            minY = blocks[i].y;
        }else{
            if(blocks[i].y < minY){
                minY = blocks[i].y;
            }
        }
    }
    return minY;
}
