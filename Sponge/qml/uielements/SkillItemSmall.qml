import VPlay 1.0
import QtQuick 1.1

Rectangle {

    property alias imageSource: skillImage.source
    property alias text: skillText.text
    property variant skillType // "jump", "water" or "stick"

    width: mySpongeScene.width / skillItemsbar.skillItemCount
    height: mySpongeScene.width / 3
    color: "lightgrey"

    Image {
        id: skillImage
        anchors.horizontalCenter: parent.horizontalCenter
        y: parent.height / 2 - parent.height / 2 / 1.5
        width: parent.height / 2
        height: parent.height / 2
    }

    Text {
        id: skillText
        color: "black"
        font.pixelSize: 10
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        font.family: fontLoader.name
        text: qsTr("SkillItem")
    }

    MouseArea {
        anchors.fill: parent
        onClicked: mySpongeScene.displaySkill(skillType);
        onPressed: parent.opacity = 0.7
        onReleased: parent.opacity = 1.0
    }

    function setUp(text, image, type) {

        skillText.text = text;
        skillImage.source = image;
        skillType = type;
    }
}
