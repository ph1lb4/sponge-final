// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Image {
    x: 0; y: 0
    height: 180
    width: 140
    source: "../images/other_skin.jpg"

    MouseArea {
        anchors.fill: parent
        onClicked: {

            spongeController.selectSkin(source);
            spongeDisplay.slideOut();
        }
    }
}
