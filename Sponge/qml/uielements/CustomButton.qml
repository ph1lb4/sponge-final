import VPlay 1.0
import QtQuick 1.1


Image {

    property alias text: textfield.text
    property alias mouseArea: mouseArea
    property alias imgSrc: img.source
    id: img

    width: 100
    height: 35

    Text {
        id: textfield
        anchors.centerIn: parent
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onPressed: parent.opacity = 0.7
        onReleased: parent.opacity = 1.0

        onCanceled: parent.opacity = 1.0

        onClicked: {
            if(window.sound){
                clickedSound.play();
            }
        }
    }

    Sound{
        id: clickedSound
        volume: 2
        source: "../sounds/click.mp3"
    }
}
