// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {

    x: 0; y: 0
    color: "lightgrey"
    width: mySpongeScene.width

    property int slideDuration: 800

    Flickable {

           id: flickable
           width: parent.width
           height: parent.height
           x: 10
           y: 30

           contentWidth: 140 * 5 + 10 * 6
           contentHeight: row.height
           flickableDirection: Flickable.HorizontalFlick

           Row {
               id: row
               width: mySpongeScene.width
               height: mySpongeScene.height
               spacing: 10


               SkinSelector {
                    source: "../images/" + getImageForIndex(1);
               }

               SkinSelector {
                    source: "../images/" + getImageForIndex(2);
               }

               SkinSelector {
                    source: "../images/" + getImageForIndex(3);
               }

               SkinSelector {
                    source: "../images/" + getImageForIndex(4);
               }

               Image {
                   x: 0; y: 0
                   height: 180
                   width: 140
                   source: "../images/buy_more_skins.jpg"

                   MouseArea {
                       anchors.fill: parent
                       onClicked: {
                           // open the store
                           window.state = "ingameStoreSceneState";
                       }
                   }
               }
           }
    }

    Behavior on x {
        SmoothedAnimation { duration: slideDuration; easing.type: Easing.InOutQuad }
      }

    function slideIn() {
       x = 0
    }

    function slideOut() {
       x = mySpongeScene.width
    }

    Image {
        width: 100; height: 40
        x: 0
        y: 230
        source: "../images/arrow.jpg"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                slideOut();
            }
        }
    }

    function getImageForIndex(index) {

        if(index < 10) {
            return "skin0" + index + ".png";
        } else {
            return "skin" + index + ".png";
        }
    }
}
