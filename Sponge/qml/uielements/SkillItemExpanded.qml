// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import VPlay 1.0

Rectangle {

    color: "#d3d3d3"
    width: mySpongeScene.width
    height: mySpongeScene.width / 3

//    property int maxWidth: skillLevelStars.width
//    property int stepSize: maxWidth / spongeController.maxSkillLevel

    property int slideDuration: 800

    SkillItemSmall {
        id: skillItem
        x: 0
        y: parent.height - height
        width: mySpongeScene.width / 3
        imageSource: "../images/skill_icon.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {

                slideOut();
                mySpongeScene.skillItembar.slideIn();
            }
        }
    }

//    Rectangle {
//        id: skillLevelIndicator
//        x: skillLevelStars.x
//        y: skillLevelStars.y
//        height: skillLevelStars.height

//        // the width of this indicator represents the skill level
//        width: stepSize * spongeController.getSkillLevel(skillItem.skillType)

//        color: "yellow"
//    }

//    Image {
//        id: skillLevelStars
//        width: 140
//        height: parent.height / 20 * 6
//        anchors.centerIn: parent
//        source: "../images/stars_empty.png"
//    }

    Grid {
        id: emptyGrid
        anchors.centerIn: parent
        columns: 5; rows: 2

        Repeater {
            model: spongeController.maxSkillLevel

            Image {
                width : 25; height: 25
                anchors.margins: 5
                source: "../images/empty_star.png"
            }

            onModelChanged: loadItemWithCocos(parent); // this shit is needed to refresh the repeater, otherwise the repeater will lose all data when changing the model
        }
    }

    Grid {
        anchors.left: emptyGrid.left
        anchors.top: emptyGrid.top
        columns: 5; rows: 2

        Repeater {
            model: spongeController.getSkillLevel(skillItem.skillType)

            Image {
                width : 25; height: 25
                anchors.margins: 5
                source: "../images/star.png"
            }

            onModelChanged: loadItemWithCocos(parent); // this shit is needed to refresh the repeater, otherwise the repeater will lose all data when changing the model
        }
    }

    // clicking this button will tell the spongecontroller to increase the skill
    Image {
        id: increaseButton
        width: 50
        height: 50
        source: "../images/plus.png"

        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.rightMargin: 10

        MouseArea {
            anchors.fill: parent
            onClicked: {
                // tell the controller to increase the skill
                spongeController.increaseSkill(skillItem.skillType);
            }
        }
    }

    Behavior on x {
        SmoothedAnimation { duration: slideDuration; easing.type: Easing.InOutQuad }
      }

    function slideIn() {
       x = 0
    }

    function slideOut() {
       x = mySpongeScene.width
    }

    // initialize the expanded skill item with its type
    function setUp(skillType) {

        if(skillType === spongeController.jumpingType) {

            skillItem.setUp("Jumping", "../images/skill_icon.png", skillType);
        } else if(skillType === spongeController.waterType) {

            skillItem.setUp("Water Resistance", "../images/skill_icon.png", skillType);
        } else if(skillType === spongeController.stickyType) {

            skillItem.setUp("Stickiness", "../images/skill_icon.png", skillType);
        }
    }
}
