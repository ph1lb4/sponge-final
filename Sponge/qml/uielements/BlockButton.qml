// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import "../entities"
import VPlay 1.0
import QtQuick 1.1
import Box2D 1.0


Block {
    entityType: "blockbutton"

    //aliases have to be specified if a BoxButton is used
    property string iconSrc;
    property alias area: area
    property string role;
    property bool isBig;

    id: blockButton
    width: isBig?110:70;
    height: isBig?110:70;

    Image {
        id: icon
        fillMode: Image.PreserveAspectFit
        height: parent.height/3*2
        anchors.centerIn: parent
        source: iconSrc
    }

    MouseArea {
        id: area
        anchors.fill: parent

        onPressed: blockButton.opacity = 0.7
        onReleased: blockButton.opacity = 1.0

        onCanceled: blockButton.opacity = 1.0

        onClicked: {
            if(window.sound){
                // soundController.play();
            }
            switch(role) {
            case "play":
                window.state = "gameSceneState";
                break;
            case "leaderboard":
                window.state = "leaderboardSceneState";
                break;
            case "sponge":
                window.state = "mySpongeSceneState";
                break;
            case "sound":
                if(window.sound) {
                    window.sound = false;
                    icon.source = "../images/sound_off.png";
                } else {
                    window.sound = true;
                    icon.source = "../images/sound_on.png";
                }
                break;
            case "store":
                window.state = "ingameStoreSceneState";
                break;
            case -1: // invalid type
                console.debug("Blockbutton init failed.");
                break;
            }
        }
    }

    bodyTypeCollider: Body.Static;
}
