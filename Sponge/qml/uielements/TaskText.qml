import VPlay 1.0
import QtQuick 1.1

Text {

    height: 50
    visible: false;
    font.pixelSize: 20

    Behavior on x {
        SmoothedAnimation { duration: 1000; easing.type: Easing.InOutQuad }
      }

    function slideMiddle() {
        visible = true;
        x = gameScene.width / 2 - width / 2;
        timerMiddle.start();
    }

    function slideOut() {
        x = -width;
        timerOut.start();
    }

    function resetToBeginning() {
        visible = false;
        x = gameScene.width;
    }

    Timer {
      id: timerMiddle
      interval: 1700

      onTriggered: {
          slideOut();
          timerMiddle.stop();
      }
    }

    Timer {
      id: timerOut
      interval: 1000

      onTriggered: {
          resetToBeginning();
          timerOut.stop();
      }
    }
}
