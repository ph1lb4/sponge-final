// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {

    width: mySpongeScene.width

    property int slideDuration: 800
    property int skillItemCount: 2 // define how many items are in the bar, this affects their size

    SkillItemSmall {
        id: jumpingItem
        x: 0
        y: parent.height - height
        imageSource: "../images/skill_icon.png"
        text: qsTr("Jumping")
        skillType: spongeController.jumpingType
    }

//    SkillItemSmall {
//        id: waterresistanceItem
//        y: parent.height - height
//        anchors.horizontalCenter: parent.horizontalCenter
//        imageSource: "../images/skill_icon.png"
//        text: qsTr("Water Resistance")
//        skillType: spongeController.waterType
//    }

    SkillItemSmall {
        id: stickinessItem
        y: parent.height - height
        anchors.right: parent.right
        imageSource: "../images/skill_icon.png"
        text: qsTr("Stickiness")
        skillType: spongeController.stickyType
    }

    Behavior on x {
        SmoothedAnimation { duration: slideDuration; easing.type: Easing.InOutQuad }
      }

    function slideIn() {
       x = 0
    }

    function slideOut() {
       x = -mySpongeScene.width
    }
}
