// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {

    width: mySpongeScene.width
    height: 100
    color: "lightgrey"

//    CustomButton {
//        anchors.top: parent.top
//        anchors.left: parent.left
//            id: buySkillPoint;
//            text: "Buy Skill Point";
//            mouseArea.onClicked: {
//                if(!window.store.supported){
//                    NativeUtils.displayMessageBox("In-App purchases not supported", "", 1);
//                }else{
//                    window.store.buyItem("skill_point_id");
//                }
//            }
//        }

    Text {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.topMargin: 5
        anchors.leftMargin: 5
        font.family: fontLoader.name
        font.pixelSize: 9
        text: "You need 5\nfragments to\nget a full\nskillpoint."
    }

    Image {
        id : emptySkillImage
        anchors.top: parent.top
        anchors.centerIn: parent
        anchors.topMargin: 5
        width: 90
        height: 90
        source: "../images/empty_star.png"
    }

    Image {
        id : skillImage
        anchors.top: parent.top
        anchors.centerIn: parent
        anchors.topMargin: 5
        width: 90
        height: 90
        source: getSourceForFragments()
        MouseArea {
            anchors.fill: parent
            onClicked: spongeController.increaseAvailableFragments(1);
        }
    }

    Image {
        anchors.top: parent.top
        anchors.right: parent.right
        width: 30
        height: 30
        source: "../images/star.png"
    }

    Text {
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 3
        text: spongeController.availableSkillPoints - spongeController.availableSkillPoints % 1 +  "x "
        font.pixelSize: 12
        horizontalAlignment: Text.AlignLeft
    }

    function getSourceForFragments() {

        var fragments = spongeController.availableFragments % 5;

        switch(fragments) {
        case 0: return "../images/empty_star.png";
            case 1: return "../images/star_1_outof_5.png";
                case 2: return "../images/star_2_outof_5.png";
                    case 3: return "../images/star_3_outof_5.png";
                        case 4: return "../images/star_4_outof_5.png";
        default: return "../images/star.png";
        }
    }
}
