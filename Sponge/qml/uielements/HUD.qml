import VPlay 1.0
import QtQuick 1.1

Image {

    source: "../images/hud.png"
    width: gameScene.width
    height: 40

    property int actualScore: 0;
    property int currentScore: 0;
    property variant activePowerUp: "none"

    //    Image {
    //        id: soakedImage
    //        anchors.verticalCenter: parent.verticalCenter
    //        x: 5
    //        width: 20; height: 20
    //        source: "../images/soakedness_health.png"
    //    }

    //    Rectangle {
    //        id: soakednessLevel
    //        anchors.verticalCenter: parent.verticalCenter
    //        anchors.left: soakedImage.right
    //        anchors.leftMargin: 5
    //        height: 15
    //        color: "lightblue"
    //        width: spongeController.health * 1.5 // size depends on healh, factor needed to fit
    //    }

    Text{
        id: actPowerUp
        anchors.verticalCenter: parent.verticalCenter
        font.family: fontLoader.name
        font.pixelSize: 11
        x: 10
        text: "Active PowerUp: "
    }

    Image{
        id: powerUp
        anchors.left: actPowerUp.right
        anchors.verticalCenter: parent.verticalCenter
        height: parent.height * 0.7
        fillMode: Image.PreserveAspectFit
        source: getPowerUpImageSource();
    }

    function getPowerUpImageSource(){
        var source = "../images/none.png";
        switch(activePowerUp){
        case "jump":
            source = "../images/jump.png";
            break;
        case "glue":
            source = "../images/glue.png";
            break;
        case "spikes":
            source = "../images/spikes.png";
            break;
        case "magnet":
            source = "../images/magnet.png";
            break;
        default:
            source = "../images/none.png";
            break;
        }
        return source;
    }

    function updateActivePowerUp(newPowerUp){
        activePowerUp = newPowerUp;
        powerUp.source = getPowerUpImageSource();
    }

    Text{
        id: currentScoreText
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: actualScoreText.left
        anchors.rightMargin: 15
        font.family: fontLoader.name
        font.pixelSize: 12
        text: currentScore
    }

    Text {
        id: actualScoreText
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 15
        font.family: fontLoader.name
        font.pixelSize: 12
        text: "Max: " + actualScore
    }

//    Text {
//        id: livesText
//        anchors.verticalCenter: parent.verticalCenter
//        anchors.right: livesImage.left
//        anchors.rightMargin: 5
//        font.family: fontLoader.name
//        font.pixelSize: 12
//        text: spongeController.lives // show the lives
//    }

//    Image {
//        id: livesImage
//        anchors.verticalCenter: parent.verticalCenter
//        anchors.right: parent.right
//        anchors.rightMargin: 5
//        width: 30; height: 30
//        source: "../images/lives.png"
//    }

    function updateCurrenScore(score){
        currentScore = score;
        currentScoreText.text = currentScore;
    }

    function updateActualScore(score){
        actualScore = score;
        actualScoreText.text = "Max: " + actualScore;
    }

    function resetScore(){
        actualScore = 0;
        currentScore = 0;
    }
}
