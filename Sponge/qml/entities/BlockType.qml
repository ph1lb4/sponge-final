import VPlay 1.0
import QtQuick 1.1
import Box2D 1.0

Item {

    property variant wood: woodType
    property variant metal: metalType
    property variant ice: iceType
    property variant stone: stoneType

    property variant selectedType;

    Item {
        id: woodType
        property double friction: 2
        property variant textureSource: "woodBlock.png"
    }

    Item {
        id: metalType
        property double friction: 1
        property variant textureSource: "metalBlock.png"
    }

    Item {
        id: iceType
        property double friction: 0.1
        property variant textureSource: "iceBlock.png"
    }

    Item {
        id: stoneType
        property double friction: 1.6
        property variant textureSource: "stoneBlock.png"
    }

}
