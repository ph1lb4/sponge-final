import VPlay 1.0
import QtQuick 1.1

EntityBase {

    property alias textureSource: entityTexture.source;

    id: entityBase

    Image {
        id: entityTexture
        width: entityBase.width
        height: entityBase.height
        anchors.centerIn: entityBase
    }
}

