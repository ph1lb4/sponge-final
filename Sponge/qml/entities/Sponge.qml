import VPlay 1.0
import QtQuick 1.1


CustomEntityBase {

    Behavior on y {
        // the cross-fade animation should last 500ms
        SmoothedAnimation {duration: 500}
    }

    property alias boxCollider: boxCollider
    property alias controllerX: twoAxisCtrl.xAxis
    property int skin: 4

    entityType: "sponge"

    width: 30
    height: 40
    textureSource: spongeController.spongeSkin

    BoxCollider {
        id: boxCollider
        categories: Box.Category2
        collidesWith: Box.Category1 | Box.Category3
        anchors.centerIn: parent
        width: parent.width - 2
        height: parent.height - 2
        density: 64
        friction: 0.1

        body.fixedRotation: true
        linearDamping: 0

        force: Qt.point(twoAxisCtrl.xAxis * 1400, 0);

        // linearVelocity: Qt.point(twoAxisCtrl.xAxis * 80, 0);

        fixture.onBeginContact: {
            if(gameScene.gameStarted){
                var component = other.parent.parent;
                gameScene.onCollisionDetected(component, contactNormal);
            }
        }

        fixture.onEndContact: {
            if(gameScene.gameStarted){
                var component = other.parent.parent;
                gameScene.onContactEnded(component);
            }
        }
    }

    TwoAxisController {
        id: twoAxisCtrl
    }
}
