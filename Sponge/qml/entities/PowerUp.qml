import VPlay 1.0
import QtQuick 1.1
import Box2D 1.0

CustomEntityBase {

    Behavior on y {
        // the cross-fade animation should last 500ms
        SmoothedAnimation {duration: 500}
    }

    property alias collider: boxCollider;   
    property variant type: "spikes";

    PowerUpType {
        id: powerUpType
        selectedType: {
            switch(type){
            case "jump":
                selectedType = jump;
                break;
            case "spikes":
                selectedType = spikes;
                break;
            case "magnet":
                selectedType = magnet;
                break;
            case "glue":
                selectedType = glue;
                break;
            }
        }
    }

    entityType: "powerup"
    width: 30
    height: 30
    textureSource: "../images/" + powerUpType.selectedType.textureSource

    BoxCollider {
        id: boxCollider
        categories: Box.Category1
        collidesWith: Box.Category1 | Box.Category2
        anchors.fill: parent

        anchors.centerIn: parent

        density: 10
        sensor: false
        friction: 0.6
        restitution: 0

        fixture.friction: 0
        fixture.restitution: 0
        fixture.density: 50
        linearDamping: 3 //is responsible for the falling-speed of the block (lower value = higher speed)
        angularDamping: 0
        body.bullet: true
        body.fixedRotation: true //prevents blocks from rotating and falling down other blocks

        //NOT SURE IF THIS IS EVEN NEEDED - maybe to prevent the already fallen block from being moved by the sponge
        fixture.onBeginContact: {
            var component = other.parent.parent;
            var collidingType = component.owningEntity.entityType;

            if(collidingType === "powerup" || collidingType === "floor") {
                //console.debug("Current Block finished falling down.");
                if(contactNormal.y < 0){
                    bodyType = Body.Static;
                }
            } else if(collidingType === "block" && contactNormal.y > 0) {
                removeEntity();
            } else if (collidingType === "sponge") {
                removeEntity();
            }
        }
    }
}
