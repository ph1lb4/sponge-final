import VPlay 1.0
import QtQuick 1.1
import Box2D 1.0

CustomEntityBase {

    Behavior on y {
        // the cross-fade animation should last 500ms
        SmoothedAnimation {duration: 500}
    }

    property alias collider: boxCollider;
    property alias bodyTypeCollider: boxCollider.bodyType

    property variant type: "stone";

    BlockType{
        id: blockType
        selectedType: {
            switch(type){
            case "stone":
                selectedType = stone;
                break;
            case "wood":
                selectedType = wood;
                break;
            case "metal":
                selectedType = metal;
                break;
            case "ice":
                selectedType = ice;
                break;
            }
        }
    }

    entityType: "block"
    width: 50
    height: 50
    textureSource: "../images/" + blockType.selectedType.textureSource

    BoxCollider {
        id: boxCollider
        categories: Box.Category1
        collidesWith: Box.Category1 | Box.Category2
        anchors.fill: parent

        height: parent.height - 2
        width: parent.width - 2

        density: 1000
        friction: blockType.selectedType.friction
        restitution: 0

        fixture.friction: 0
        fixture.restitution: 0
        fixture.density: 50
        linearDamping: 5 //is responsible for the falling-speed of the block (lower value = higher speed)
        angularDamping: 0
        body.bullet: true
        body.fixedRotation: true //prevents blocks from rotating and falling down other blocks

        //NOT SURE IF THIS IS EVEN NEEDED - maybe to prevent the already fallen block from being moved by the sponge
        fixture.onBeginContact: {
            var component = other.parent.parent;
            var collidingType = component.owningEntity.entityType;

            if(collidingType === "floor" || collidingType === "block") {
                console.debug("Current Block finished falling down.");
                if(contactNormal.y < 0){
                    bodyType = Body.Static;
                    console.debug("BLOCK BODY TYPE TO STATIC!!!!!!!!!")
                    if(window.sound){
                        collisionSound.play();
                    }
                }
            }
        }
    }

    // gets played at a collision
    Sound {
        id: collisionSound
        volume: 2
        source: "../sounds/block_collision2.wav"
    }
}
