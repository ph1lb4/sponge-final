import VPlay 1.0
import QtQuick 1.1
import Box2D 1.0

Item {

    property variant jump: jumpType
    property variant spikes: spikesType
    property variant magnet: magnetType
    property variant glue: glueType

    property variant selectedType;

    Item {
        id: jumpType
        property variant textureSource: "jump.png"
    }

    Item {
        id: spikesType
        property variant textureSource: "spikes.png"
    }

    Item {
        id: magnetType
        property variant textureSource: "magnet.png"
    }

    Item {
        id: glueType
        property variant textureSource: "glue.png"
    }

}
