import VPlay 1.0
import QtQuick 1.1

CustomEntityBase {

    entityType: "water"
    textureSource: "../images/water.png"

    width: 320
    height: 480


    Behavior on y {
        NumberAnimation {duration: 500}
    }

    Behavior on x {
        NumberAnimation {duration: 500}
    }

    BoxCollider{
        collisionTestingOnlyMode: true
        anchors.centerIn: parent
        width: parent.width
        height: parent.height - 25
        categories: Box.Category3
        collidesWith: Box.Category2
        sensor: true
    }
}
