import VPlay 1.0
import QtQuick 1.1
import Box2D 1.0

CustomEntityBase {

    Behavior on y {
        // the cross-fade animation should last 500ms
        SmoothedAnimation {duration: 500}
    }

    entityType: "floor"

    width: gameScene.width
    height: 20

    BoxCollider {
        id: boxCollider
        categories: Box.Category1
        collidesWith: Box.Category1 | Box.Category2
        bodyType: Body.Static
        friction: 2;
        anchors.centerIn: parent
        width: parent.width
        height: parent.height - 10
    }
}
