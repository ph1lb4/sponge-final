import VPlay 1.0
import QtQuick 1.1

SceneBase {

    onBackPressed: {
      window.state = "mainMenuState"
    }

    Text {
        id: highScore
        anchors.horizontalCenter: parent.horizontalCenter
        y: 20
        color: "black"
        text: "Your highscore: " + spongeController.loadHighScore();
        font.family: fontLoader.name
        font.pixelSize: 14
    }

    Image {
        id: trophy
        source: "../images/trophy.png"
        anchors.centerIn: parent
    }

    SimpleButton {
        width: 200
        id: resetBtn
        text: "Reset Highscore"
        font.family: fontLoader.name
        font.pixelSize: 13
        anchors.horizontalCenter: parent.horizontalCenter
        y: parent.height - 100
        onClicked: {
            spongeController.storeHighScore(0);
            updateHighscore();
        }
    }

    SimpleButton {
        width: 200
        text: "Back"
        font.family: fontLoader.name
        font.pixelSize: 13
        anchors.horizontalCenter: parent.horizontalCenter
        y: parent.height - 50
        onClicked: {
            console.debug("GAME OVER OK CLICKED");
            window.state = "mainMenuState";
        }
    }

    function updateHighscore(){
        highScore.text = "Your highscore: " + spongeController.loadHighScore();
    }
}
