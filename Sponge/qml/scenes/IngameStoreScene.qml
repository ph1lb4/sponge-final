import VPlay 1.0
import QtQuick 1.1
import "../uielements"

SceneBase {

    onBackPressed: {
      window.state = "mainMenuState"
    }

    Rectangle {
        anchors.centerIn: parent
        width: 50
        height: 50
        color: "white"
    }

    CustomButton{
        id: buySkillPoint;
        text: "Buy Skill Point";
        mouseArea.onClicked: {
            if(!window.store.supported){
                NativeUtils.displayMessageBox("In-App purchases not supported", "", 1);
            }else{
                window.store.buyItem("skill_point_id");
            }
        }
    }
}
