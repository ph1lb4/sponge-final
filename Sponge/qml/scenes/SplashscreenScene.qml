import VPlay 1.0
import QtQuick 1.1

SceneBase {

    state: "entered"

    Image {
      id: xxmassimage
      anchors.fill: parent
      source: "../images/splashscreen.png"
      z: -1
    }

    Timer {
      id: loadingTimer
      interval: 2000

      onTriggered: {
        window.state = "mainMenuState"
      }
    }

    states: [
      State {
        name: "entered"
        StateChangeScript {
          script: {
              loadingTimer.start();
          }
        }
      }
   ]
}
