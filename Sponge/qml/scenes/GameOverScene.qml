import VPlay 1.0
import QtQuick 1.1

SceneBase {

    onBackPressed: {
      window.state = "mainMenuState"
    }

    Text {
        id: highScore
        anchors.horizontalCenter: parent.horizontalCenter
        y: 50
        color: "black"
        text: "YOUR SCORE: " + gameScene.score;
        font.family: fontLoader.name
        font.pixelSize: 14
    }

    Image {
        id: rip
        source: "../images/rip.png"
        width: parent.width - 10
        fillMode: Image.PreserveAspectFit
        anchors.centerIn: parent
    }

    SimpleButton {
        id: mainmenuButton
        width: 150
        text: "Main Menu"
        font.family: fontLoader.name
        font.pixelSize: 13
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: rip.bottom
        anchors.topMargin: 10
        onClicked: {
            window.state = "mainMenuState";
        }
    }

    SimpleButton {
        width: 150
        text: "Retry"
        font.family: fontLoader.name
        font.pixelSize: 13
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: mainmenuButton.bottom
        anchors.topMargin: 5
        onClicked: {
            window.state = "gameSceneState";
        }
    }


    onVisibleChanged: {
        if(visible){
            if(gameCenter.authenticated){
                if(currentHighscore > gameScene.score){
                    currentHighscore = gameScene.score;
                    window.reportScore(gameScene.score, "sponge_leaderboard");
                }
            }
        }
    }
}
