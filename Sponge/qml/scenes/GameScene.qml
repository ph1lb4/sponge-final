import VPlay 1.0
import QtQuick 1.1
import "../entities"
import "../uielements"
import "../levels"
import "../controllers"
import "../js/GameLogic.js" as GameLogic
import "../js/SpawnLogic.js" as SpawnLogic

SceneBase {

    property alias level: loader.loadedLevel;
    property alias playerSponge: playerSponge;
    property bool spongeCanJump: true;
    property bool gameStarted: false;
    property alias floor: floor;
    property alias waterTimer: waterTimer
    property alias water: water;
    property int score: 0;
    property bool isGameOver: false;
    property bool newHighScoreTriggered: false;

    property int waterXOffset: 0

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        onClicked: {
            jump();
        }
    }

    onBackPressed: {
        window.state = "mainMenuState"
    }

    PhysicsWorld {
        id: physicsWorld
        z: 1
        height: parent.height
        updatesPerSecondForPhysics: 120
        velocityIterations: 1 //1
        positionIterations: 0 //0
    }


    Sponge{
        id: playerSponge
        controllerX: 0
        x: gameScene.width / 2 - width / 2
        y: gameScene.height * 0.6

        onXChanged: {
            // check if sponge moves out of screen
            if(x > gameScene.width - 3){
                console.debug("SPONGE X ",x," flipping sponge to left side.");
                x = -(width - 5);
            }else if(x < -(width - 3)){
                console.debug("SPONGE X ",x," flipping sponge to right side.");
                x = gameScene.width - 5;
            }
        }

        onYChanged: {
            //GameLogic.checkSpongeY();
            if(y > gameScene.height){
                gameOver();
            }

            GameLogic.getCurrentScore();
        }
    }

    Floor{
        id:floor

        anchors.left: parent.left
        y: parent.height - floor.height
        height: parent.height * 0.2
        width: parent.width
    }

    Water{
        id: water
        x: 0
        y: parent.height - 20;
        z: 999
        height: parent.height;
        width: parent.width * 2
    }

    Timer{
        id: waterTimer
        interval: 550
        repeat: true
        running: false

        onTriggered: {
            water.y -= gameScene.height * 0.017

            if(waterXOffset === 0){
                waterXOffset = -gameScene.width/25;
            }else{
                waterXOffset = 0;
            }

            water.x = waterXOffset;
        }
    }

    MultiResolutionImage{
        id: backgroundImage
        anchors.centerIn: parent
        z: -1;
    }

    ParallaxScrollingBackground {
        movementVelocity: Qt.point(12,0)
        ratio: Qt.point(1.0,1.0)
        sourceImage: "../images/clouds4-sd.png"
        running: true
        z:-1
    }

    ParallaxScrollingBackground {
        movementVelocity: Qt.point(40,0)
        ratio: Qt.point(1.0,1.0)
        sourceImage: "../images/clouds3-sd.png"
        running: true
        z:-1
    }

    HUD {
        id: hud
        anchors.left: parent.left
        anchors.top: parent.top
        z: 1000;
    }

    LevelLoader {
        id: loader
        anchors.fill: parent
    }

    Timer {
        id: spawnBlockTimer
        interval: 2000
        repeat: true
        running: false

        onTriggered: {
            spawnBlock();
        }
    }

    TaskText {
        id: taskText
        anchors.verticalCenter: parent.verticalCenter
        font.family: fontLoader.name
        text: "ESCAPE THE WATER!"
        x: gameScene.width
    }

    Timer {
        id: spawnPowerupTimer
        interval: 5000
        repeat: true
        running: false

        onTriggered: {
            SpawnLogic.spawnPowerup();
            // calculate a new random spawn interval between 8 and 18 seconds
            interval = SpawnLogic.calculateNewRandomInterval(8000, 18000);
        }
    }

    function spawnBlock() {
        SpawnLogic.checkAndSpawn();
    }

    function loadLevel(levelname){
        loader.levelSource = levelname;
        floor.textureSource = loader.loadedLevel.floorTextureSource;
        backgroundImage.source = loader.loadedLevel.backgroundSource;
        physicsWorld.gravity.y = loader.loadedLevel.gravity;
    }

    function startLevel(){
        playerSponge.boxCollider.bodyType = Body.Dynamic;
        GameLogic.startGame();
        spawnBlockTimer.start();
        spawnPowerupTimer.start();
        waterTimer.start();
        isGameOver = false;
        spongeCanJump = true;

        taskText.slideMiddle();
        state = "playing";
    }

    function gameOver(){
        if(window.sound){
            soundController.gameOverSound.play()
        }
        playerSponge.boxCollider.bodyType = Body.Static;
        gameOverText.opacity = 1;
        gameStarted = false;
        gameOverTimer.start();
        waterTimer.stop();
        isGameOver = true;
        state = "gameOver";
        spawnBlockTimer.stop();
        spawnPowerupTimer.stop();
        newHighScoreTriggered = false;
        powerUpTimer.stop();
        hud.updateActivePowerUp("none");

        var highScore = spongeController.loadHighScore();
        console.debug("PREVIOUSLY SAVED HIGHSCORE FROM SETTINGS: ", highScore);
        if(highScore === "undefined"){
            highScore = 0;
        }
        if(gameScene.score > highScore){
            highScore = gameScene.score;
            spongeController.storeHighScore(highScore);
            leaderboardScene.updateHighscore();
        }
    }

    function reset(){
        // remove all entities of type "block" and powerup
        entityManager.removeEntitiesByFilter(["block", "powerup"]);
        // reset floor
        floor.y = gameScene.height - floor.height;
        // reset initial falldown flag
        GameLogic.reset();
        // reset HUD Points
        hud.resetScore();
        isGameOver = false;
        gameOverText.opacity = 0;
    }

    Text {
        id: gameOverText
        text: qsTr("GAME OVER!")
        anchors.centerIn: parent
        color: "red"
        font.family: fontLoader.name
        font.pixelSize: 18
        z: 1000
        opacity: 0
    }

    Timer {
        id: gameOverTimer
        interval: 2000
        repeat: false
        onTriggered: {
            window.state = "gameOverSceneState";
            gameScene.water.y = gameScene.height;
        }
    }

    Image {
        id: newHighScore
        anchors.centerIn: parent
        width: gameScene.width / 2
        fillMode: Image.PreserveAspectFit
        source: "../images/newHighScore.png"
        opacity: 0
        z: 1100
    }

    PropertyAnimation {
        id: animateOpacity
        target: newHighScore
        properties: "opacity"
        from: 0
        to: 0.70
        duration: 2500
        loops: 1

        onCompleted: {
            hideNewHighScore.start();
            animateOpacity.stop();
            animateSize.stop();
        }
    }

    PropertyAnimation {
        id: hideNewHighScore
        target: newHighScore
        properties: "opacity"
        to: 0
        duration: 500
        loops: 1

        onCompleted: {
            hideNewHighScore.stop();
        }
    }

    PropertyAnimation {
        id: animateSize
        target: newHighScore
        properties: "width"
        from: gameScene.width / 2
        to: gameScene.width * 0.8
        duration: 2500
        loops: 1
    }

    function newHighScoreAnimation(){
        if(!newHighScoreTriggered){
            console.debug("##############!!!!!!!!!! NEW HIGHSCORE - STARTING ANIMATION !!!!!!!!!!!!!!!################")
            animateOpacity.start();
            animateSize.start();
            newHighScoreTriggered = true;
        }
    }

    function jump(){
        GameLogic.jump();
    }

    function xMovement(value){
        GameLogic.xMovement(value);
    }

    function onCollisionDetected(component, contactNormal){
        GameLogic.onCollisionDetected(component, contactNormal);
        var collidingType = component.owningEntity.entityType;
        if(collidingType === "powerup"){
            if(powerUpTimer.running){
                powerUpTimer.restart();
            }else{
                powerUpTimer.start();
            }
        }
    }

    Timer{
        id: powerUpTimer
        interval: 10000
        repeat: false
        running: false

        onTriggered: {
            powerUpTimer.stop();
            spongeController.activePowerUp = "none";
            hud.updateActivePowerUp("none");
        }
    }

    function onContactEnded(component){
        GameLogic.onContactEnded(component);
    }
    function updateCurrentScore(score){
        hud.updateCurrenScore(score);
    }
    function updateActualScore(score){
        hud.updateActualScore(score);
        var highscore = settings.getValue("highscore");
        if(highscore !== 0 && highscore !== "undefined" && score > highscore){
            newHighScoreAnimation();
        }
    }

    function blockCollision(){
        if(window.sound){
            soundController.collisionSound.play();
        }
    }

    function powerUpCollected(powerUp){
        hud.updateActivePowerUp(powerUp);
    }

    function levelMovement(){
        levelMoveTimer.start();
    }

    Timer{
        id: levelMoveTimer
        interval: 600
        running: false
        repeat: false

        onTriggered: {
            GameLogic.levelMoveDone();
            levelMoveTimer.stop();
        }
    }

    states: [
        State {
            name: "playing"
            StateChangeScript {
                script: {
                    //restartButton.visible = false;
                }
            }
        },
        State {
            name: "gameOver"
            StateChangeScript {
                script: {
                    //                    restartButton.visible = true;
                    //                    if(window.sound){
                    //                        gameOverSound.play()
                    //                    }
                    //                    spawnBlockTimer.stop();
                    //                    spawnPowerupTimer.stop();
                }
            }
        }
    ]
}
