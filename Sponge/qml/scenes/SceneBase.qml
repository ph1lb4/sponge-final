import VPlay 1.0
import QtQuick 1.1

Scene {
    height: 480
    width: 320

    function show() {
        opacity = 1;
    }

    function hide() {
        opacity = 0;
    }

    function funnyMenuClouds(){
        parallex1.opacity = !parallex1.opacity;
        parallex2.opacity = !parallex2.opacity;
        parallex3.opacity = !parallex3.opacity;
        parallex4.opacity = !parallex4.opacity;
    }

    // this is an important performance improvement, as renderer can skip invisible items (and all its children)
    // also, the focus-property for key handling relies on the visible-property
    visible: opacity>0


    // handle this signal in each Scene
    signal backPressed

    // this fades in and out automatically, when the opacity gets changed from 0 to 1 in ChickenOutbreakMain
    Behavior on opacity {
        // the cross-fade animation should last 350ms
        NumberAnimation { duration: 350 }
    }

    MultiResolutionImage{
        id: backgroundImage
        anchors.centerIn: parent
        source: "../images/earthBackground-sd.png"
        z: -1;
    }

    ParallaxScrollingBackground {
        id: parallex1
        movementVelocity: Qt.point(12,0)
        ratio: Qt.point(1.0,1.0)
        sourceImage: "../images/clouds-sd.png"
        running: true
        opacity: 0
        z:-1
    }

    ParallaxScrollingBackground {
        id: parallex2
        movementVelocity: Qt.point(40,0)
        ratio: Qt.point(1.0,1.0)
        sourceImage: "../images/clouds2-sd.png"
        running: true
        opacity: 0;
        z:-1
    }

    ParallaxScrollingBackground {
        id: parallex3
        movementVelocity: Qt.point(12,0)
        ratio: Qt.point(1.0,1.0)
        sourceImage: "../images/clouds3-sd.png"
        running: true
        z:-1
    }

    ParallaxScrollingBackground {
        id: parallex4
        movementVelocity: Qt.point(40,0)
        ratio: Qt.point(1.0,1.0)
        sourceImage: "../images/clouds4-sd.png"
        running: true
        z:-1
    }

    Keys.onPressed: {
        //console.debug("SceneBase: pressed key code: ", event.key)

        // only simulate on desktop platforms!
        if(event.key === Qt.Key_Backspace && system.desktopPlatform) {
            //console.debug("backspace key pressed - simulate a back key pressed on desktop platforms for debugging the user flow of Android on windows!")
            backPressed()
        }
    }

    Keys.onBackPressed: {
        // handles the Android back button
        backPressed()
    }

    Rectangle {
        color: "black"
        width: parent.width
        height: 500
        x: 0
        y: -height
        z: 1010
    }

    Rectangle {
        color: "black"
        width: parent.width
        height: 500
        x: 0
        y: parent.height
        z: 1010
    }
}
