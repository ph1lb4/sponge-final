import VPlay 1.0
import QtQuick 1.1
import "../uielements"
import "../images"

SceneBase {

    property alias skillItembar: skillItemsbar

    onBackPressed: {
      window.state = "mainMenuState"
    }

    MultiResolutionImage{
        id: backgroundImage
        anchors.centerIn: parent
        z: -1;
        source: "../images/background-hd.png"

        MouseArea {
            anchors.fill: parent
            onClicked: spongeController.reset();
        }
    }

    AvailableSkillPointsDisplay {
        id: availableSkillsDisplay
        x: 0
        y: 0
    }

    Image {
        id: spongeImage
        anchors.centerIn: parent
        width: parent.width / 2
        height: parent.height / 10 * 4
        source: spongeController.spongeSkin

        MouseArea {
            anchors.fill: parent
            onClicked: {

                //spongeController.reset();
                spongeDisplay.slideIn();
            }
        }
    }

    SpongeDisplay {
        id: spongeDisplay
        anchors.top: availableSkillsDisplay.bottom
        anchors.bottom: skillItemsbar.top
        x: mySpongeScene.width
        y: availableSkillsDisplay.height
        height: 250
        z:2
    }

    SimpleButton {
        width: 100
        height: 30
        id: resetBtn
        text: "Back"
        font.family: fontLoader.name
        font.pixelSize: 13
        anchors.left: parent.left
        y: parent.height - 105 - height;
        z: 1
        onClicked: {
            window.state = "mainMenuState";
        }
    }

    SkillItemsBar {
        id: skillItemsbar
        x: 0
        y: parent.height - height
    }

    SkillItemExpanded {
        id: skillItemExpanded
        x: mySpongeScene.width
        y: mySpongeScene.height - height
    }

    function displaySkill(skillType) {

        console.debug("SkillItem clicked, displaying skill: " + skillType);

        skillItemExpanded.setUp(skillType);
        skillItemExpanded.slideIn();
        skillItemsbar.slideOut();
    }
}
