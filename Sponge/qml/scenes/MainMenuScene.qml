import VPlay 1.0
import QtQuick 1.1
import "../entities"
import "../uielements"
import "../js/GameLogic.js" as GameLogic

SceneBase {
    property int easterEggCount: 0

    id: mainMenu
    property int noOfButtons: 5
    //    property alias physicsEnabled: physicsWorld.enabled
    //    property bool spongeCanJump: true;
    //    property alias playerSponge: playerSponge


    //floorTextures get loaded, every time opacity changes
    //needed to have always the floor-texture from the gamescene
    onOpacityChanged: {
        if(opacity == 1) {
            loadLvlBehaviour();
            createMenuButtons();
        } else if(opacity == 0) {
            removeMenuButtons();
        }
    }

    Floor {
        id: menuFloor
        x: 0
        y: parent.height - height;

        MouseArea{
            anchors.fill: parent
            //anchors.centerIn: parent

            onClicked: {
                easterEggCount++;
                console.debug("EASTER EGG COUNT: ",easterEggCount);
                if(easterEggCount === 10){
                    console.debug("EASTEREGG triggered!! :-D");
                    funnyMenuClouds();
                    easterEggCount = 0;
                }
            }
        }
    }

    onBackPressed: {
        // instead of immediately shutting down the app, ask the user if he really wants to exit the app with a native dialog
        nativeUtils.displayMessageBox(qsTr("Really quit the game?"), "", 2);
    }

    Connections {
        target: nativeUtils

        onMessageBoxFinished: {
            if(accepted){
                Qt.quit();
            }
        }
    }

    function loadLvlBehaviour() {
        //        if(gameScene.level !== null) {
        //            menuFloor.textureSource = gameScene.level.floorTextureSource;
        //            physicsWorld.gravity.y = gameScene.level.gravity;
        //            //console.debug("++++++++++++++++++++++++GAMELEVEL NOT NULL!")
        //        } else {
        //testfloor
        menuFloor.textureSource = "../images/menu_ground.png"
        physicsWorld.gravity.y = -9.81 //earth gravity
        //}

    }

    function createMenuButtons() {
        var buttonProperties = 0;
        for (var i = 0; i < noOfButtons; i++) {
            switch(i) {
            case 0:
                buttonProperties = {x: 40, y: 60, role: "play", type: "wood", isBig: true, iconSrc: "../images/play.png"};
                break;
            case 1:
                buttonProperties = {x: 180, y: 30, role: "leaderboard", type: "metal", isBig: false, iconSrc: "../images/trophy.png"};
                break;
            case 2:
                buttonProperties = {x: 100, y: 340, role: "sponge", type: "wood", isBig: true, iconSrc: spongeController.spongeSkin}
                break;
            case 3:
                buttonProperties = {x: 200, y: 210, role: "sound", type: "ice", isBig: true, iconSrc: window.sound?"../images/sound_on.png":"../images/sound_off.png"}
                break;
            case 4:
                buttonProperties = {x: 50, y: 200, role: "store", type: "stone", isBig: false, iconSrc: "../images/buy_more_skins.jpg"}
                break;
            case -1: // invalid type
                console.debug("blockButton init failed.");
                break;
            }
            mainMenuEntityManager.createEntityFromUrlWithProperties(Qt.resolvedUrl("../uielements/BlockButton.qml"),buttonProperties);
        }
    }

    function removeMenuButtons() {
        if (mainMenuEntityManager !== null) {
            mainMenuEntityManager.removeAllEntities();
        }
    }


    //    MouseArea {
    //        id: mouseArea
    //        anchors.fill: parent

    //        onClicked: {
    //            jump();
    //        }
    //    }

    //    Sponge {
    //        id: playerSponge
    //        controllerX: 0
    //        // anchors.bottom: menuFloor.top;
    //        y: 300
    //        anchors.horizontalCenter: menuFloor.horizontalCenter;

    //        onXChanged: {
    //            // check if sponge moves out of screen
    //            if(x > mainMenuScene.width - 3){
    //                console.debug("SPONGE X ",x," flipping sponge to left side.");
    //                x = -(width - 5);
    //            }else if(x < -(width - 3)){
    //                console.debug("SPONGE X ",x," flipping sponge to right side.");
    //                x = mainMenuScene.width - 5;
    //            }
    //        }
    //    }

    //    PhysicsWorld {
    //        id: physicsWorld
    //        anchors.centerIn: parent
    //        anchors.fill: parent
    //        gravity.y: -10
    //        z: 1
    //        height: parent.height
    //        updatesPerSecondForPhysics: 120
    //        velocityIterations: 1 //1
    //        positionIterations: 0 //0
    //    }

    //    function jump(){

    //        //if(spongeCanJump){
    //            spongeCanJump = false;
    //            var jumpVal = 1;
    //            var impulsePow = 0;

    //            if(gameScene.level !== null) {
    //                impulsePow = gameScene.level.gravity * 390 * jumpVal;
    //            } else {
    //                impulsePow = physicsWorld.gravity.y * 390 * jumpVal;
    //            }

    //            var impulse = playerSponge.boxCollider.body.getWorldVector(Qt.point(0,impulsePow));
    //            playerSponge.boxCollider.applyLinearImpulse(impulse, playerSponge.boxCollider.body.getWorldCenter());
    //        //}

    //    }
}
