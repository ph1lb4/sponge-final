Philipp Baldauf (25 hours):
In-App-Purchases
(Concept, Prices, Items, iTunes Connect, Implementation)
10 hours

Sounds
(Recording, Research, Implementation)
3 hours

Highscore
(Concept, Implementation)
3 hours

GameLogic
(Bug Fixing)
3 hours

Meetings
6 hours

########################################################################################################

Simon Kapl (25 hours):
Menu Structure
(Concept, Design, Research, Navigation, Implementation)
6 hours

Playable Main Menu (not in final version)
8 hours

PowerUps
3 hours

Balancing, Testing
2 hours

Meetings
6 hours

########################################################################################################

Philipp Jahoda (26 hours):
Data Management
(Concept, Research, Implementation, Save and Restore, Stats)
14 hours

Sponge Controller:
(UI, Skill Management, Distribution, Concepts from In-App-Purchases partly Implemented)
8 hours

Meetings
4 hours

########################################################################################################

Peter Neunteufel (29 hours):
Game Logic
(Concept, Implementation, Bug Fixing, Spawn Logic, Level transition)
20 hours

Design Prototypes:
3 hours

Meetings
6 hours

